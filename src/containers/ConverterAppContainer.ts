import { connect } from "react-redux";
import Converter from "./../components/Converter/Converter";
import {
  currencyChanged,
  amountChanged,
  exchangePressed,
  currencySwitched,
  loadRates,
} from "../redux/modules/converter";
import {
  ContainerState,
  ContainerDispatchProps
} from "./../redux/modules/interfaces";
import { Currency } from "ts-money";

function mapStateToProps(state: any) {
  return {
    converter: state.converter
  } as ContainerState;
}

function mapDispatchToProps(dispatch: any) {
  return {
    currencyChanged: (fromChanged: boolean, currency: Currency) =>
      dispatch(currencyChanged(fromChanged, currency)),
    amountChanged: (amount: number) => dispatch(amountChanged(amount)),
    exchangePressed: () => dispatch(exchangePressed()),
    currencySwitched: () => dispatch(currencySwitched()),
    loadRates: (base: Currency, currencies: Array<Currency>) => dispatch(loadRates(base, currencies)),
  } as ContainerDispatchProps;
}

export default connect(mapStateToProps, mapDispatchToProps)(Converter);
