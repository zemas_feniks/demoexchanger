import { Money, Currencies, Currency } from "ts-money";
import { AnyAction } from "redux";
import { ConverterState, PocketInterface } from "./interfaces";
import requestRates from "./../../api/api";
import { getRate, getConvertedMoney } from "../../utils/money";

const CURRENCY_CHANGED = "CURRENCY_CHANGED";
const AMOUNT_CHANGED = "AMOUNT_CHANGED";
const EXCHANGE_PRESSED = "EXCHANGE_PRESSED";
const CURRENCY_SWITCHED = "CURRENCY_SWITCHED";
const LOAD_RATES = "LOAD_RATES";
const LOAD_RATES_FAILED = "LOAD_RATES_FAILED";
const LOAD_RATES_SUCCESS = "LOAD_RATES_SUCCESS";

export function loadRates(base: Currency, currencies: Array<Currency>) {
  return function(dispatch: any) {
    requestRates(base, currencies)
      .then(ratesMap => {
        dispatch(loadRatesSuccess(ratesMap));
      })
      .catch(e => {
        dispatch(loadRatesFailed(e));
      });
  };
}

function loadRatesFailed(error: string) {
  return {
    type: LOAD_RATES_FAILED,
    error: error
  };
}

function loadRatesSuccess(rates: Map<Currency, number>) {
  return {
    type: LOAD_RATES_SUCCESS,
    rates
  };
}

export function currencyChanged(fromChanged: boolean, currency: Currency) {
  return {
    type: CURRENCY_CHANGED,
    fromChanged,
    currency
  };
}

export function amountChanged(amount: number) {
  return {
    type: AMOUNT_CHANGED,
    amount
  };
}

export function exchangePressed() {
  return {
    type: EXCHANGE_PRESSED
  };
}

export function currencySwitched() {
  return {
    type: CURRENCY_SWITCHED
  };
}

const initialPockets: Map<Currency, PocketInterface> = new Map([
  [
    Currencies.USD,
    {
      id: 1,
      balance: new Money(10000, Currencies.USD)
    }
  ],
  [
    Currencies.EUR,
    {
      id: 2,
      balance: new Money(11000, Currencies.EUR)
    }
  ],
  [
    Currencies.GBP,
    {
      id: 3,
      balance: new Money(12000, Currencies.GBP)
    }
  ]
]);

const initialState = {
  currencies: [Currencies.USD, Currencies.EUR, Currencies.GBP],
  pockets: initialPockets,
  exchangeFromCurrency: Currencies.USD,
  exchangeToCurrency: Currencies.EUR,
  amountToExchange: 0,
  isExchangeDataLoaded: false,
  rates: new Map<Currency, number>(),
  loadingError: "",
  isLoadingFailed: false
} as ConverterState;

export default function reducer(state = initialState, action: AnyAction) {
  switch (action.type) {
    case CURRENCY_CHANGED:
      const field = action.fromChanged
        ? "exchangeFromCurrency"
        : "exchangeToCurrency";
      return {
        ...state,
        [field]: action.currency
      };
    case AMOUNT_CHANGED:
      return {
        ...state,
        amountToExchange: action.amount
      };
    case CURRENCY_SWITCHED:
      return {
        ...state,
        exchangeFromCurrency: state.exchangeToCurrency,
        exchangeToCurrency: state.exchangeFromCurrency
      };
    case EXCHANGE_PRESSED:
      const rate = getRate(
        state.exchangeFromCurrency,
        state.exchangeToCurrency,
        state.rates
      );

      const fromMoney = Money.fromDecimal(
        state.amountToExchange,
        state.exchangeFromCurrency,
        Math.floor
      );
      const toMoney = getConvertedMoney(
        state.amountToExchange,
        state.exchangeFromCurrency,
        state.exchangeToCurrency,
        rate
      );

      const fromPocket = state.pockets.get(
        state.exchangeFromCurrency
      ) as PocketInterface;
      const toPocket = state.pockets.get(
        state.exchangeToCurrency
      ) as PocketInterface;
      const updatedPockets = new Map<Currency, PocketInterface>([
        [
          state.exchangeFromCurrency,
          { id: fromPocket.id, balance: fromPocket.balance.subtract(fromMoney) }
        ],
        [
          state.exchangeToCurrency,
          { id: toPocket.id, balance: toPocket.balance.add(toMoney) }
        ]
      ]);
      const newPockets = new Map([
        ...Array.from(state.pockets.entries()),
        ...Array.from(updatedPockets.entries())
      ]);
      return {
        ...state,
        pockets: newPockets
      };
    case LOAD_RATES:
      return {
        ...state
      };
    case LOAD_RATES_SUCCESS:
      return {
        ...state,
        loadingError: "",
        isLoadingFailed: false,
        isExchangeDataLoaded: true,
        rates: action.rates
      };
    case LOAD_RATES_FAILED:
      return {
        ...state,
        isLoadingFailed: true,
        loadingError: action.error,
        isExchangeDataLoaded: true
      };
    default:
      return {
        ...state
      };
  }
}
