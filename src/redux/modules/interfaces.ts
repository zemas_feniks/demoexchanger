import { Money, Currencies, Currency } from "ts-money";

export interface ConverterState {
  currencies: Array<Currency>;
  pockets: Map<Currency, PocketInterface>;
  exchangeFromCurrency: Currency;
  exchangeToCurrency: Currency;
  amountToExchange: number;
  isExchangeDataLoaded: boolean;
  rates: Map<Currency, number>;
  loadingError: string;
  isLoadingFailed: boolean;
}

export interface ContainerState {
  converter: ConverterState;
}

export interface ContainerDispatchProps {
  currencyChanged: (fromChanged: boolean, currency: Currency) => void;
  amountChanged: (amount: number) => void;
  exchangePressed: () => void;
  currencySwitched: () => void;
  // loadRatesFailed: (error: string) => void,
  // loadRatesSuccess: (base: Currency, currencies: Array<Currency>) => void,
  loadRates: (base: Currency, currencies: Array<Currency>) => void,
}

export interface PocketInterface {
  id: number;
  balance: Money;
}
