import { createStore, applyMiddleware, combineReducers } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk'
import converter from './modules/converter';


const loggerMiddleware = createLogger();
const createStoreWithMiddleware = applyMiddleware(thunk, loggerMiddleware)(createStore);
const reducer = combineReducers({
  converter
});

const configureStore = (initialState?: any) => createStoreWithMiddleware(reducer, initialState);
export default configureStore;

