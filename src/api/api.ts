import { Currency } from "ts-money";

const requestRates = async (base: Currency, currencies: Array<Currency>) => {
  const filteredCurrencies = currencies.filter(currency => {
    return currency.code !== base.code;
  });
  const filteredCurrenciesCodes = filteredCurrencies.map(c => c.code);
  const symbolsUri = filteredCurrenciesCodes.length
    ? `&symbols=${filteredCurrenciesCodes.join(",")}`
    : ``;
  const apiUrl = `https://api.exchangeratesapi.io/latest?base=${base.code}${symbolsUri}`;

  return fetch(apiUrl, {
    mode: "cors"
  })
    .then(response => {
      if (!response.ok) {
        throw new Error(response.statusText);
      }
      
      return response.json();
    })
    .then(data => {
      return currencies.reduce((ratesMap, currency) => {
        if (typeof data.rates[currency.code] !== undefined) {
          ratesMap.set(currency, data.rates[currency.code]);
        }

        return ratesMap;
      }, new Map<Currency, number>());
    });
};

export default requestRates;
