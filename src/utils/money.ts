import { Currency, Money } from "ts-money"

export const getConvertedMoney = (amount: number, fromCurrency: Currency, toCurrency: Currency, rate: number):Money => {
  return Money.fromDecimal(
    amount,
    toCurrency,
    Math.floor
  ).multiply(rate, Math.floor);
};

export const getRate = (fromCurrency: Currency, toCurrency: Currency, rates: Map<Currency, number>): number => {
  return fromCurrency === toCurrency ? 1 : rates.get(toCurrency) || 0;
}
