import React, { useEffect } from "react";
import {
  ContainerDispatchProps,
  ContainerState,
  PocketInterface
} from "./../../redux/modules/interfaces";
import { Currency } from "ts-money";
import MoneyInput from "./../MoneyInput/MoneyInput";
import "./Converter.css";
import Pocket from "../Pocket/Pocket";
import { Paper, Fab, IconButton, CircularProgress } from "@material-ui/core";
import ImportExportIcon from "@material-ui/icons/ImportExport";
import ExhangeRate from "./../ExchangeRate/ExchangeRate";
import { getRate } from "../../utils/money";
import { getConvertedMoney } from "./../../utils/money";

interface ConverterProperties extends ContainerState, ContainerDispatchProps {}

const Converter: React.FC<ConverterProperties> = ({
  converter,
  currencyChanged,
  amountChanged,
  exchangePressed,
  currencySwitched,
  loadRates
}) => {
  // TODO: after rerendering of component timer setups again
  // need to avoid new timers on every rerendering
  useEffect(() => {
    let intervalId = setInterval(() => {
      loadRates(converter.exchangeFromCurrency, converter.currencies);
    }, 10000);
    return () => {
      clearInterval(intervalId);
    };
  });

  if (!converter.isExchangeDataLoaded) {
    loadRates(converter.exchangeFromCurrency, converter.currencies);
    return (
      <Paper className="converter">
        <CircularProgress color="secondary" />
      </Paper>
    );
  }

  const fromPocket = converter.pockets.get(
    converter.exchangeFromCurrency
  ) as PocketInterface;

  const toPocket = converter.pockets.get(
    converter.exchangeToCurrency
  ) as PocketInterface;

  const switchCurrencies = () => {
    const newCurrency = converter.exchangeToCurrency;
    currencySwitched();
    loadRates(newCurrency, converter.currencies);
  };

  const fromCurrencyChanged = (newCurrency: Currency) => {
    if (newCurrency === converter.exchangeToCurrency) {
      switchCurrencies();
    } else {
      currencyChanged(true, newCurrency);
      loadRates(newCurrency, converter.currencies);
    }
  };

  const toCurrencyChanged = (newCurrency: Currency) => {
    currencyChanged(false, newCurrency);
  };

  const rate = getRate(
    converter.exchangeFromCurrency,
    converter.exchangeToCurrency,
    converter.rates
  );

  const toMoney = getConvertedMoney(
    converter.amountToExchange,
    converter.exchangeFromCurrency,
    converter.exchangeToCurrency,
    rate
  );

  const isExchangePossible = () => {
    return (
      converter.isExchangeDataLoaded &&
      !converter.isLoadingFailed &&
      converter.amountToExchange > 0 &&
      toMoney.toDecimal() > 0 &&
      converter.amountToExchange <= fromPocket.balance.toDecimal()
    );
  };

  return (
    <Paper className="converter">
      <div className="converter-top">
        <MoneyInput
          currencies={converter.currencies}
          currency={converter.exchangeFromCurrency}
          amount={converter.amountToExchange}
          onCurrencyChanged={fromCurrencyChanged}
          onAmountChanged={amountChanged}
        />

        <Pocket pocket={fromPocket} />

        <IconButton
          size="small"
          color="primary"
          aria-label="switch currencies"
          className="btn btn-switch"
          onClick={switchCurrencies}
        >
          <ImportExportIcon fontSize="inherit" />
        </IconButton>

        <ExhangeRate
          fromCurrency={converter.exchangeFromCurrency}
          toCurrency={converter.exchangeToCurrency}
          byRate={converter.rates.get(converter.exchangeToCurrency) || 0}
        />
      </div>

      <div className="converter-bottom">
        <MoneyInput
          currencies={converter.currencies}
          currency={converter.exchangeToCurrency}
          amount={toMoney.toDecimal()}
          onCurrencyChanged={toCurrencyChanged}
          onAmountChanged={() => {}}
        />
        <Pocket pocket={toPocket} />
        <Fab
          variant="extended"
          size="small"
          color="primary"
          aria-label="Exchange"
          className="btn btn-primary btn-exchange"
          disabled={!isExchangePossible()}
          onClick={exchangePressed}
        >
          Exchange
        </Fab>
      </div>
    </Paper>
  );
};

export default Converter;
