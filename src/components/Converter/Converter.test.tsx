import React from "react";
import ReactDOM from "react-dom";
import { act } from "react-dom/test-utils";
import { render, unmountComponentAtNode } from "react-dom";
import Converter from "./Converter";
import { Currencies, Money, Currency } from "ts-money";
import { fireEvent } from '@testing-library/react';
import {
  ConverterState,
  PocketInterface,
  ContainerState
} from "./../../redux/modules/interfaces";

let initialState: any = null;
let container: any = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);

  const initialPockets: Map<Currency, PocketInterface> = new Map([
    [
      Currencies.USD,
      {
        id: 1,
        balance: new Money(10000, Currencies.USD)
      }
    ],
    [
      Currencies.EUR,
      {
        id: 2,
        balance: new Money(11000, Currencies.EUR)
      }
    ],
    [
      Currencies.GBP,
      {
        id: 3,
        balance: new Money(12000, Currencies.GBP)
      }
    ]
  ]);

  const rates = new Map([
    [Currencies.EUR, 0.9023523],
    [Currencies.GBP, 0.80235235]
  ]);

  initialState = {
    currencies: [Currencies.USD, Currencies.EUR, Currencies.GBP],
    pockets: initialPockets,
    exchangeFromCurrency: Currencies.USD,
    exchangeToCurrency: Currencies.EUR,
    amountToExchange: 0,
    isExchangeDataLoaded: true,
    rates: rates,
    loadingError: "",
    isLoadingFailed: false
  } as ConverterState;
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
  initialState = null;
  // cleanup();
});

it("exchanges money", async () => {
  const loadRates = jest.fn();
  const currencyChanged = jest.fn();
  const amountChanged = jest.fn();
  const exchangePressed = jest.fn();
  const currencySwitched = jest.fn();
  const rates = {
    EUR: 0.9023523,
    GBP: 0.80235235
  }; // `{"rates":{"EUR":0.8979078747,"GBP":0.7592708988},"base":"USD","date":"2019-12-12"}`

  const spy = jest.spyOn(global, "fetch").mockImplementation(() =>
    Promise.resolve({
      ok: true,
      json: () => {
        return {
          rates: rates
        };
      }
    })
  );

  const stateProps = {
    converter: initialState
  } as ContainerState;

  await act(async () => {
    render(
      <Converter
        converter={initialState}
        loadRates={loadRates}
        currencyChanged={currencyChanged}
        amountChanged={amountChanged}
        exchangePressed={exchangePressed}
        currencySwitched={currencySwitched}
      />,
      container
    );
  });


  expect(loadRates).toHaveBeenCalledTimes(0);
  expect(container.querySelector(".btn-exchange").textContent).toBe("Exchange");

  act(() => {

    
    // container.querySelector(".converter-top .MuiOutlinedInput-inputAdornedStart").value = 10;
    const fromInput = container.querySelector(".converter-top .MuiOutlinedInput-inputAdornedStart");
    fireEvent.change(fromInput, { target: { value: "10" } });
  });

  expect(container.querySelector(".converter-top .MuiOutlinedInput-inputAdornedStart").value).toBe("10");
  expect(amountChanged).toHaveBeenCalledTimes(1);
  // expect(container.querySelector(".converter-bottom .MuiOutlinedInput-inputAdornedStart").value).toBe("0.90");

  global.fetch.mockRestore();
});
