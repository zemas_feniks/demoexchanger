import React from "react";
import { PocketInterface } from "../../redux/modules/interfaces";
import "./Pocket.css";

interface PocketComponentInterface {
  pocket: PocketInterface;
}

const Pocket: React.FC<PocketComponentInterface> = ({ pocket }) => {
  return (
    <div className="pocket">
      Balance: {pocket.balance.getCurrencyInfo().symbol}{pocket.balance.toString()} 
    </div>
  );
};

export default Pocket;
