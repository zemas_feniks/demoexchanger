import React from "react";
import { Provider } from "react-redux";
import configureStore from "./../../redux/configureStore";
import ConverterAppContainer from "../../containers/ConverterAppContainer";
import "./App.css";

const store = configureStore();

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <div className="app">
        <ConverterAppContainer />
      </div>
    </Provider>
  );
};

export default App;
