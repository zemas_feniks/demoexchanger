import React from "react";
import ReactDOM from "react-dom";
import { act } from "react-dom/test-utils";
import { render, unmountComponentAtNode } from "react-dom";
import App from "./App";


let container: any = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
  // cleanup();
});

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<App />, div);
});

it("renders rates data", async () => {
  const rates = {
    EUR: 0.9023523,
    GBP: 0.80235235
  }; // `{"rates":{"EUR":0.8979078747,"GBP":0.7592708988},"base":"USD","date":"2019-12-12"}`

  const spy = jest.spyOn(global, "fetch").mockImplementation(() =>
    Promise.resolve({
      ok: true,
      json: () => {
        return {
          rates: rates
        };
      }
    })
  );

  await act(async () => {
    render(<App />, container);
  });

  expect(spy).toHaveBeenCalled();
  expect(container.querySelector(".btn-exchange").textContent).toBe("Exchange");
  expect(container.querySelector(".converter .exhange-rate").textContent).toBe(
    "$1 = €0.9023"
  );
  global.fetch.mockRestore();
});
