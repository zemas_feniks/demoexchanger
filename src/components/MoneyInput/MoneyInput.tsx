import React from "react";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import InputAdornment from "@material-ui/core/InputAdornment";
import { Currencies, Currency, Money } from "ts-money";
import "./MoneyInput.css";
import MaskedInput from "./MaskedInput";

interface MoneyInputProperties {
  currencies: Array<Currency>;
  currency: Currency;
  amount: number;
  onCurrencyChanged: (newCurrecny: Currency) => void;
  onAmountChanged: (amount: number) => void;
}

const MoneyInput: React.FC<MoneyInputProperties> = ({
  currencies,
  currency,
  amount,
  onCurrencyChanged,
  onAmountChanged
}) => {
  const onChange = (event: any) => {
    const parsedValue = parseFloat(event.target.value as string);
    const filteredValue = isNaN(parsedValue) ? 0 : parsedValue;
    const money = Money.fromDecimal(filteredValue, currency, Math.floor);
    onAmountChanged(money.toDecimal());
  };

  return (
    <div className="money-input">
      <FormControl variant="outlined" className="currency-control">
        <Select
          value={currency.code}
          onChange={event => {
            const currencyCode: keyof typeof Currencies = event.target
              .value as keyof typeof Currencies;
            onCurrencyChanged(Currencies[currencyCode]);
          }}
        >
          {currencies.map(currencyItem => {
            return (
              <MenuItem key={currencyItem.code} value={currencyItem.code}>
                {currencyItem.code}
              </MenuItem>
            );
          })}
        </Select>
      </FormControl>
      <FormControl variant="outlined">
        <OutlinedInput
          startAdornment={
            <InputAdornment position="start">{currency.symbol}</InputAdornment>
          }
          labelWidth={0}
          value={amount}
          onChange={onChange}
          id="formatted-numberformat-input"
          inputComponent={MaskedInput as any}
        />
      </FormControl>
    </div>
  );
};

export default MoneyInput;
