import React from "react";
import { Currency } from 'ts-money';
import { Money } from 'ts-money';

interface ExhangeRateProps {
  fromCurrency: Currency;
  toCurrency: Currency
  byRate: number
};

const ExhangeRate: React.FC<ExhangeRateProps> = ({ fromCurrency, toCurrency, byRate }) => {
  const rate = fromCurrency === toCurrency ? 1 : byRate;
  const roundedRate = Math.floor(rate*10000) / 10000;
  const label = rate === 0 ? `No actual rate` : `${fromCurrency.symbol}1 = ${toCurrency.symbol}${roundedRate}`;
  return (
    <div className="exhange-rate">
      {label}
    </div>
  );
};

export default ExhangeRate;
